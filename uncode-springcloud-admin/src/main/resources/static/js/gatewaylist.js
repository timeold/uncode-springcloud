﻿//@ sourceURL=gatewaylist.js
layui.config({
    base: '../../lib/' //指定 winui 路径
    , version: '1.0.0-beta'
}).extend({
    winui: 'winui/winui',
    window: 'winui/js/winui.window'
}).define(['table', 'jquery', 'winui', 'window', 'layer'], function (exports) {

    winui.renderColor();

    var table = layui.table,
        $ = layui.$, tableId = 'tableid';
    //桌面显示提示消息的函数
    var msg = top.winui.window.msg;

  	//表格渲染
   	table.render({
        id: tableId,
        elem: '#gareways',
        url: '/gateway/instances',
        page: false,
        cols: [[
            { field: 'url', title: '群集网关节点', width: 450 },
            { title: '操作', fixed: 'right', align: 'center', toolbar: '#barMenu', width: 450 }
        ]]
    });

    //表格刷新
    function reloadTable() {
        table.reload(tableId, {});
    }

    //监听工具条
    table.on('tool(menuSettingTable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值
        var tr = obj.tr; //获得当前行 tr 的DOM对象

        var ids = '';   //选中的Id
        $(data).each(function (index, item) {
            ids += item.id + ',';
        });
        if (layEvent === 'refresh') { //刷新
        	refresh(obj);
        } else if (layEvent === 'forceRefresh') { //强刷新
        	forceRefresh(obj);
        } else if (layEvent === 'query') { //查看
        	queryGateway(data);
        } else if (layEvent === 'canaryRefresh') { //查看
        	canaryRefresh(obj);
        } else if (layEvent === 'queryCanary') { //查看
        	queryCanary(data);
        }
    });
  	//刷新菜单
    function refresh(obj) {
        var mg = obj ? '确认刷新路由规则【' + obj.data.url + '】吗？' : '确认刷新选中数据吗？';
        top.winui.window.confirm(mg, { icon: 3, title: '刷新路由规则' }, function (index) {
            layer.close(index);
            top.winui.window.open({
                id: 'gatewayRefresh',
                type: 2,
                title: '刷新路由规则',
                content: obj.data.url + '/route/refresh',
                area: ['50vw', '70vh'],
                offset: ['15vh', '25vw'],
            });
        });
    }
  	//强刷新菜单
    function forceRefresh(obj) {
        var mg = obj ? '确认强制刷新路由规则【' + obj.data.url + '】吗？' : '确认刷新选中数据吗？';
        top.winui.window.confirm(mg, { icon: 3, title: '强制刷新路由规则' }, function (index) {
            layer.close(index);
            top.winui.window.open({
                id: 'gatewayForceRefresh',
                type: 2,
                title: '刷新路由规则',
                content: obj.data.url + '/route/forceRefresh',
                area: ['50vw', '70vh'],
                offset: ['15vh', '25vw'],
            });
        });
    }
  	//强刷新菜单
    function canaryRefresh(obj) {
        var mg = obj ? '确认刷新灰度规则【' + obj.data.url + '】吗？' : '确认刷新选中数据吗？';
        top.winui.window.confirm(mg, { icon: 3, title: '刷新灰度规则' }, function (index) {
            layer.close(index);
            top.winui.window.open({
                id: 'canaryRefresh',
                type: 2,
                title: '刷新灰度规则',
                content: obj.data.url + '/route/canaryRefresh',
                area: ['50vw', '70vh'],
                offset: ['15vh', '25vw'],
            });
        });
    }
  	//打开编辑窗口
    function queryGateway(tdata) {
        if (!tdata) return;
        var index = layer.load(1);
        layer.close(index);
        top.winui.window.open({
            id: 'gatewayRoutes',
            type: 2,
            title: '查看路由规则',
            content: tdata.url+'/route/all',
            area: ['50vw', '70vh'],
            offset: ['15vh', '25vw'],
        });
    }
  //打开编辑窗口
    function queryCanary(tdata) {
        if (!tdata) return;
        var index = layer.load(1);
        layer.close(index);
        top.winui.window.open({
            id: 'gatewayCanary',
            type: 2,
            title: '查看灰度信息',
            content: tdata.url+'/route/canaryInfo',
            area: ['50vw', '70vh'],
            offset: ['15vh', '25vw'],
        });
    }
    //绑定工具栏刷新按钮事件
    $('#reloadTable').on('click', reloadTable);

    exports('gatewaylist', {});
});
