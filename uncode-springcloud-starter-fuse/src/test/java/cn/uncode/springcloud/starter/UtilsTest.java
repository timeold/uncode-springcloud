
package cn.uncode.springcloud.starter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

/**
 * 
 * @author juny
 * @date 2019年1月18日
 *
 */
public class UtilsTest {
    

    @Test
    public void optionalTest() {
    	int k = "1.0.2.RC".lastIndexOf("RC");
    	int l = "1.0.2.RC".length()-"RC".length();
        Optional<String> optional = Optional.ofNullable("");
        System.out.println(k+"-"+l);
    }
    
    @Test
    public void stringTest() {
    	String fd = "user[1]";
		int fdIndex = 0;
		int start = fd.indexOf("[");
		int end = fd.indexOf("]");
		if(start != 0) {
			fdIndex = Integer.parseInt(fd.substring(start+1, end));
			fd = fd.substring(0, start);
		}
		System.out.println(fd+"-"+fdIndex);
		
    }
    
    @Test
    public void springelTest() {
    	/**
    	 * 表达式处理
    	 */
    	
    	Map<String, Object> params = new HashMap<>();
    	params.put("name", "xxxxxxxxxx");
    	Map<String, Integer> age = new HashMap<>();
    	age.put("age", 88);
    	params.put("me", age);
    	List<Map<?,?>> requestMap = new ArrayList<>();
    	requestMap.add(params);
    	Map<String, Object> params2 = new HashMap<>();
    	params2.put("name", "22222222");
    	Map<String, Integer> age2 = new HashMap<>();
    	age2.put("age", 99);
    	params2.put("me", age2);
    	requestMap.add(params2);
        String text = "用户名称是：{in:name}, 年龄：{in:me.age}";
        String text2 = "用户名称是：{in:name[1]}, 年龄：{in:me[1].age}";
        String rt = elFormart2(text, requestMap, requestMap);
        String rt2 = elFormart2(text2, requestMap, requestMap);
        System.out.println(rt);
        System.out.println(rt2);
//        int index = "in:".indexOf(":");
//		String type = "in:dsaf".substring(0, index);
//		System.out.println(type);
//		System.out.println("in:dsaf".substring(index+1));
    }
    
    
 public String elFormart2(String text, List<Map<?,?>> requestMap, List<Map<?,?>> responseMap) {
	 String rtText = text;
    	Pattern pattern = Pattern.compile("\\{[a-z]+:[A-Za-z0-9\\[\\].]+\\}");
    	Matcher matcher = pattern.matcher(text);
    	//循环，字符串中有多少个符合的，就循环多少次
    	while(matcher.find()){
    		//每一个符合正则的字符串
    		String e = matcher.group();
    		//截取出括号中的内容
    		int index = e.indexOf(":");
    		String type = e.substring(1, index);
    		String substring = e.substring(index+1, e.length()-1);
    		//取值
    		String[] vals = substring.split("\\.");
    		Object value = null;
    		for(int i = 0; i < vals.length; i++) {
    			String fd = vals[i];
    			int fdIndex = 0;
    			int start = fd.indexOf("[");
    			int end = fd.indexOf("]");
    			if(start != -1) {
    				fdIndex = Integer.parseInt(fd.substring(start+1, end));
    				fd = fd.substring(0, start);
    			}
    			if("in".equals(type.toLowerCase())) {
    				if(i == 0) {
    					value = requestMap.get(fdIndex).get(fd);
    				}else {
    					if(fdIndex > 0) {
    						List<Map<?,?>> list = (List<Map<?,?>>)value;
    						value = list.get(fdIndex).get(fd);
    					}else {
    						Map<?,?> list = (Map<?,?>)value;
    						value = list.get(fd);
    					}
    				}
    			}else {
    				if(i == 0) {
    					value = responseMap.get(fdIndex).get(fd);
    				}else {
    					if(fdIndex > 0) {
    						List<Map<?,?>> list = (List<Map<?,?>>)value;
    						value = list.get(fdIndex).get(fd);
    					}else {
    						Map<?,?> list = (Map<?,?>)value;
    						value = list.get(fd);
    					}
    				}
    			}
    			if(i == vals.length - 1) {
    				rtText = rtText.replace(e, String.valueOf(value));
    			}
    		}
    	}
    	return rtText;
 }
    
    
    public String elFormart(String text, String name, String type, Map<?,?> map) {
    	for(Map.Entry<?, ?> entry:map.entrySet()){
    		String nameKey = name == null ? String.valueOf(entry.getKey()) : name + "." + String.valueOf(entry.getKey());
    		if(entry.getValue() instanceof Map) {
    			Map<?,?> mapKey = (Map<?, ?>) entry.getValue();
    			return elFormart(text, nameKey, type, mapKey);
    		}else {
    			text = text.replaceAll("\\{" + type + nameKey + "}", String.valueOf(entry.getValue()));  
    		}
        }
    	return text;
    }

}