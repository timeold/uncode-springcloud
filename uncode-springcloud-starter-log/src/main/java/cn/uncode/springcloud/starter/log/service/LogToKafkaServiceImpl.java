package cn.uncode.springcloud.starter.log.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import lombok.extern.slf4j.Slf4j;


/**
 * 发送日志到消息队列的实现类
 * 
 * @author juny
 * @date 2019年4月23日
 *
 */
@Slf4j
public class LogToKafkaServiceImpl implements LogService {

    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Autowired(required = false)
    private KafkaTemplate<String, Object> template;

    @Override
    public void sendMsg(LogStroeModel sendingLog) {
    	if(null != template) {
    		executorService.execute(() -> {
                try {
                    template.send(LOG_TOPIC, LOG_TOPIC_KEY, sendingLog);
                } catch (Exception e) {
                    log.error("记录普通日志到kafka错误!", e);
                }
            });
    	}
    }



}
